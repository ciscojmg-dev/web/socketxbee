# Requerimientos
## Setup
  - [nodejs](https://nodejs.org/es/download/)
  - [xctu](https://www.digi.com/resources/documentation/digidocs/90001526/tasks/t_download_and_install_xctu.htm)

## Hardware
  - Cualquier Xbee:
    - Xbee Celular 
      - nano SIM
    - Xbee Wifi
  - Sockets Xbee
    - Cable USB

## Debug
  - Abre un socket por un determinado puerto.
  - Hace un "echo" a todo lo que trasmitan por ese puerto y le llega al mismo dispositivo que hizo la trasmision. 

## Recursos
  - En la carpeta `frameXbee` encontraras un ejemplo de una trama xbeeWifi que la podras utilizar con el XCTU.


   
